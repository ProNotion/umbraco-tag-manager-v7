﻿angular.module("umbraco").controller("TagManager.TagManagerEditController",
	function ($scope, $routeParams, TagManagerResource, notificationsService, navigationService) {

	    TagManagerResource.getById($routeParams.id).then(function (response) {
	        $scope.cmsTags = response.data;
	        $scope.selectedTag = $routeParams.id;
	    });

	    $scope.save = function (cmsTags) {
	        TagManagerResource.save(cmsTags).then(function (response) {
	            $scope.cmsTags = response.data;
	            notificationsService.success("Success", cmsTags.tag + " has been saved");
	            navigationService.syncTree({ tree: 'TagManager', path: content.path, forceReload: false, activate: true });
	        });
	    };

	    $scope.deleteTag = function (cmsTags) {
	        TagManagerResource.deleteTag(cmsTags).then(function (response) {
	            $scope.cmsTags = response.data;
	            notificationsService.success("Success", cmsTags.tag + " has been deleted.");
	            navigationService.syncTree({ tree: 'TagManager', path: content.path, forceReload: false, activate: true });
	            treeService.removeNode($scope.currentNode);
	        });
	        
	    };
	});