angular.module("umbraco.resources").factory("personResource", function($http){
	return{
		getById: function(id){
			return $http.get("backoffice/People/PersonApi/GetById?id="+id);
		},

		getall: function(){
		    return $http.get("backoffice/People/PersonApi/GetAll");
		},

		save: function(person){
		    return $http.post("backoffice/People/PersonApi/PostSave", angular.toJson(person));
		},

		getDrunkById: function(id){
		    return $http.get("backoffice/People/PersonApi/GetDrunk?id=" + id);
		},

		getSoberById: function(id){
		    return $http.get("backoffice/People/PersonApi/GetSober?id=" + id);
		}
	};
});