﻿using umbraco.businesslogic;
using umbraco.interfaces;

namespace Yoyocms.Umbraco7.TagManager
{
    [Application("TagManager", "Tag Manager", "icon-battery-low", 15)]
    public class TagManagerApplication : IApplication
    {
    }
}